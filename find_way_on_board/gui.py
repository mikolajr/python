import find_way
from tkinter import *

pos = []
board = []
c = []
b = []
width = 400
height = 400
step = 50
margin = 0.1


def initialize():
    global pos, board
    pos = []
    board = [[0 for x in range(width // step)] for y in range(height // step)]


def ball(x, y, color):
    t = "tag-%s,%s" % (x, y)
    c.create_oval(x*step + step*margin, y*step + step*margin, (x+1)*step - step*margin, (y+1)*step - step*margin,
                  fill=color, tags=t)


def erase(x, y):
    t = "tag-%s,%s" % (x, y)
    c.delete(t)


def click(event):
    x = event.x // step
    y = event.y // step
    if (x, y) in pos:
        return
    board[y][x] = 1 - board[y][x]

    if board[y][x] == 1:
        ball(x, y, '#000fff000')
    else:
        erase(x, y)


def start_stop(event):
    ex = event.x // step
    ey = event.y // step
    if not board[ey][ex] == 0:
        return

    for p in pos:
        x, y = p
        erase(x, y)

    pos.append((ex, ey))
    while len(pos) > 2:
        pos.pop(0)

    for p in pos:
        x, y = p
        ball(x, y, '#fff000000')


def solve():
    result = find_way.find_way_on_board(board, pos[0], pos[1])
    find_way.dump(board, result)

    for p in result:
        x, y = p
        ball(x, y, '#000000fff')

    b.configure(text="Reset", command=reset)


def reset():
    initialize()
    for y in range(len(board)):
        for x in range(len(board[y])):
            erase(x, y)
    for p in pos:
        x, y = p
        erase(x, y)
    b.configure(text="Solve", command=solve)


initialize()
master = Tk()

c = Canvas(master, width=width, height=height)
c.pack()

for x in range(0, width, step):
    c.create_line(x, 0, x, height)
for y in range(0, height, step):
    c.create_line(0, y, width, y)

c.bind("<Button-1>", click)
c.bind("<Button-2>", start_stop)

b = Button(master, text="Solve", command=solve)
b.pack()

mainloop()
