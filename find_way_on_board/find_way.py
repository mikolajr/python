
# finds the shortest path between start_pos and end_pos on a given board. path turns by 90 degrees
# (not diagonally)
# note: board is being indexed like board[y][x]
# @board     - two-dimensional integer array, where 0 specifies empty spot, non 0 an obstacle,
#              0,0 is top left corner (read only)
# @start_pos - tuple with x,y coordinates of starting position
# @end_pos   - tuple with x,y coordinates of destination position
# @return    - list of (x,y) tuples with coordinates of path's segments, or empty list if solution was not found
def find_way_on_board(board, start_pos, end_pos):
    # set with (x,y) tuples of already visited places on board, so we won't examine them again
    visited = set()

    # creates new possible path if element at given position is an empty and unvisited place
    # @pos  - tuple with x,y coordinates of new element
    # @path - existing path
    def create_new_path(pos, path):
        if pos in visited:
            return []
        if pos[0] < 0 or pos[1] < 0 or pos[1] >= len(board) or pos[0] >= len(board[0]):
            return []
        if not board[pos[1]][pos[0]] == 0:
            return []

        visited.add(pos)
        possible_path = list(path)
        possible_path.append(pos)
        return possible_path

    # list of possible paths. each path is a list of (x,y) tuples
    paths = []

    # let's start with very first element
    paths.append(create_new_path(start_pos, []))

    # repeat while there are paths to be considered
    while len(paths) > 0:
        # new paths that we will create during this iteration
        new_paths = []

        # let's check every path
        for path in paths:
            # is it the destination element?
            candidate = path[-1]
            if candidate == end_pos:
                return path

            # let's create new paths, going into four directions from current position
            new_paths.append(create_new_path((candidate[0]-1, candidate[1]), path))
            new_paths.append(create_new_path((candidate[0]+1, candidate[1]), path))
            new_paths.append(create_new_path((candidate[0], candidate[1]-1), path))
            new_paths.append(create_new_path((candidate[0], candidate[1]+1), path))

        # finally, let's replace old paths with new ones
        paths = list(filter(lambda l: len(l) > 0, new_paths))
        paths.sort(key=lambda l: len(l))

    # nothing found :-(
    return []


def dump(b, path):
    for y, row in enumerate(b):
        for x, col in enumerate(row):
            c = " ."
            if col > 0:
                c = " *"
            else:
                t = (x, y)
                if path.count(t) > 0:
                    i = path.index(t)
                    c = ""
                    if i < 10:
                        c = " "
                    c += str(path.index(t))

            print(c, " ", end="")
        print("")


if __name__ == "__main__":
    b = [[0 for x in range(6)] for y in range(6)]
    b[1][1] = 1
    b[4][1] = 1
    b[2][3] = 1
    b[3][3] = 1
    b[5][3] = 1
    b[2][4] = 1
    b[2][5] = 1
    b[3][2] = 1

    result = []
    result = find_way_on_board(b, (5, 0), (4, 5))
    print(result)
    dump(b, result)
