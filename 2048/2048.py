from enum import Enum
from random import *

class Direction(Enum):
    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4

    def value_of(s):
        dispatcher = {'u': Direction.UP, 'd': Direction.DOWN,
            'l': Direction.LEFT, 'r': Direction.RIGHT}
        if s in dispatcher:
            return dispatcher[s]
        return None

class Board:
    score = 0
    board = []

    def __init__(self):
        self.initialize()

    def initialize(self):
        self.score = 0
        self.board = list()
        for i in range(4):
            self.board.append(list())
            for j in range(4):
                self.board[i].append(0)

    # just dumps board into string
    def dump(self):
        result = "Score: {:d}\n".format(self.score)
        for row in self.board:
            for column in row:
                if column == 0:
                    result += "   . "
                else:
                    result += "{:4d} ".format(column)
            result = result + "\n"
        return result

    # sets row to given list
    def set_row(self, row_num, row):
        for i in range(len(row)):
            self.board[row_num][i] = row[i]

    # sets column to given list
    def set_col(self, col_num, col):
        for i in range(len(col)):
            self.board[i][col_num] = col[i]

    def get_row(self, row_num):
        return list(self.board[row_num])

    def get_col(self, col_num):
        result = list()
        for i in range(len(self.board)):
            result.append(self.board[i][col_num])
        return result

    # shifts and adds numbers in the list to the beginning of the list
    # returns new list
    def shift(self, nums):
        # remove empty spaces, effectively shift to the left
        result = filter(lambda x: x > 0, nums)

        result = list(result)

        # compresss/add duplicate elements
        i = 0
        while i < len(result)-1:
            if (result[i] == result[i+1]):
                self.score += result[i]
                result[i] = 2 * result[i];
                result.pop(i+1)
            i += 1

        # pad right with empty elements
        result.extend([0] * (len(nums) - len(result)))

        return result

    def make_move(self, dir):
        if dir == Direction.LEFT:
            for i in range(len(self.board)):
                row = self.get_row(i)
                row = self.shift(row)
                self.set_row(i, row)
        elif dir == Direction.RIGHT:
            for i in range(len(self.board)):
                row = self.get_row(i)
                row.reverse()
                row = self.shift(row)
                row.reverse()
                self.set_row(i, row)
        elif dir == Direction.UP:
            for i in range(len(self.board)):
                col = self.get_col(i)
                col = self.shift(col)
                self.set_col(i, col)
        elif dir == Direction.DOWN:
            for i in range(len(self.board)):
                col = self.get_col(i)
                col.reverse()
                col = self.shift(col)
                col.reverse()
                self.set_col(i, col)

        self.place_new()

    # places new random element (2 or 4) in random empty slot
    def place_new(self):
        empties = list()
        size = len(self.board)

        for row in range(size):
            for col in range(size):
                if self.board[row][col] == 0:
                    empties.append((row, col))

        random_value = (randint(1,100) % 2 + 1) * 2
        row, col = choice(empties)

        self.board[row][col] = random_value
        return

    def is_game_over(self):
        def contains_duplicates(row):
            i = 0
            while (i < len(row)-1):
                if row[i] == row[i+1]:
                    return True
                i += 1
            return False

        for row in self.board:
            for value in row:
                if value == 0:
                    return False

        for row_idx in range(len(self.board)):
            row = self.get_row(row_idx)
            if contains_duplicates(row):
                return False

        for col_idx in range(len(self.board)):
            col = self.get_col(col_idx)
            if contains_duplicates(col):
                return False

        return True

    def run_game(self):
        self.initialize()
        self.place_new()
        self.place_new()
        while not self.is_game_over():
            print(self.dump())

            dir = input("qudlr? ")
            if "q" == dir:
                break
            dir = Direction.value_of(dir)
            if dir is not None:
                self.make_move(dir)

        print("\n\nG A M E  O V E R!!!")
        print(b.dump())
        return

b = Board()
b.run_game()

'''
b = Board()
print(b.dump())

b.set_row(1, [0, 2, 0, 8])
b.set_col(0, [2, 0, 4, 2])
print(b.dump())

for i in range(4):
    b.set_row(i, b.get_row(i))
    b.set_col(i, b.get_col(i))
print(b.dump())

b.make_move(Direction.LEFT)
print(b.dump())
b.make_move(Direction.RIGHT)
print(b.dump())
b.make_move(Direction.UP)
print(b.dump())
b.make_move(Direction.DOWN)
print(b.dump())

# print(b.shift([0, 2, 0, 8]))
# print(b.shift([0, 2, 2, 4]))
# print(b.shift([0, 4, 2, 2]))
# print(b.shift(b.shift(b.shift([0, 4, 2, 2]))))

print(b.dump())
b.make_move(Direction.LEFT)
print(b.dump())
b.make_move(Direction.UP)
print(b.dump())
b.make_move(Direction.RIGHT)
print(b.dump())
b.make_move(Direction.LEFT)
print(b.dump())

print(b.is_game_over())

b.set_row(0, [2, 2, 4, 8])
b.set_row(1, [4, 2, 4, 2])
b.set_row(2, [2, 2, 4, 8])
b.set_row(3, [2, 2, 4, 8])
print(b.is_game_over())

b.set_row(0, [2, 4, 8, 16])
b.set_row(1, [4, 8, 16, 32])
b.set_row(2, [32, 16, 8, 4])
b.set_row(3, [16, 8, 4, 2])

print(b.is_game_over())
'''
