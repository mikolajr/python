import itertools

# We pass in a list of votes and we are returned a list of names in the descending order of the score that each candidate received.
# A voter is allowed to vote for up to three different candidates. The order of the votes is important. The first vote that a voter places is worth three points. The second vote is worth two points. The third vote is worth one point.
# The function should return a list of candidates in descending order of the total number of points received by the candidate.


# add index to each vote -> filter to up to max -> flat map -> aggregate by candidate -> sort
def find_winners(votes):
    max_no_of_votes = 3

    # add no of points for vote and limit no of votes per votee
    votes = [zip(votee, range(max_no_of_votes, 0, -1)) for votee in votes]

    # flatten the list
    votes = [v for votee in votes for v in votee]


    by_names = {}

    # sort by candidate
    votes = sorted(votes, key=lambda v: v[0])
    # group by candidates
    for candidate, votes in itertools.groupby(votes, lambda v: v[0]):
        by_names[candidate] = sum([v[1] for v in votes])

    winners = sorted(by_names.keys(), key=lambda k: by_names[k])
    winners.reverse()

    return winners


if __name__ == "__main__":
    print(find_winners([['A', 'B', 'C'], ['B', 'C', 'D', 'A'], ['C', 'B']]))
